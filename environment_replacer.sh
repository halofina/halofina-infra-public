#!/bin/bash
level=""
if [ "${1}" != "" ]; then
	level="_${1}"
fi

list=$( cat  .env )
for entry in ${list}; do
	envkey=${entry%=*}
	envval=$(printenv ${envkey}${level})
	keyval="${envkey}=${envval}"
        envfile=$envfile$keyval$'\n'
done

echo "${envfile}" > ".env.parsed"
