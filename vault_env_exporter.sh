#!/bin/bash

function validate(){
    if [ -z  $2 ]; then
        echo "${1} is empty"
        exit 1
    else
        echo "${1} is set"
    fi
}

validate "VAULT_USER" $VAULT_USER
validate "VAULT_PASS" $VAULT_PASS
validate "VAULT_URL" $VAULT_URL
validate "VAULT_KV" $VAULT_KV
echo VAULT_KV=$VAULT_KV
validate "VAULT_APP" $VAULT_APP
echo VAULT_APP=$VAULT_APP

# VAULT Login
VAULT_TOKEN=$(curl -k -s --request POST --data "{\"password\": \"$VAULT_PASS\"}" ${VAULT_URL}/v1/auth/userpass/login/${VAULT_USER} | jq '.auth.client_token' |  tr -d '"')

echo $VAULT_TOKEN

# VAULT Get Secret
# VAULT using KV v1
# echo "Fetch Environment From ${VAULT_URL}/v1/${VAULT_KV}/${VAULT_APP}"
#curl -s --header "X-Vault-Token: ${VAULT_TOKEN}" ${VAULT_URL}/v1/${VAULT_KV}/${VAULT_APP} | jq '.data' > env.json
#cat env.json

# VAULT using KV v2
echo "Fetch Environment From ${VAULT_URL}/v1/${VAULT_KV}/data/${VAULT_APP}"
curl -k -s --header "X-Vault-Token: ${VAULT_TOKEN}" ${VAULT_URL}/v1/${VAULT_KV}/data/${VAULT_APP} | jq '.data.data' > env.json
cat env.json

# Export Secret To ENV Variable
arr=()
while IFS='' read -r line; do
   arr+=("$line")
done < <(jq 'keys[]' env.json)

for key in "${arr[@]}"
do
    key=$(echo $key |  tr -d '"')
    echo $key
    val=$(jq ".$key" env.json | tr -d '"')
    echo $val
    export $key=$val
done

# Cleanup
rm -rf env.json
